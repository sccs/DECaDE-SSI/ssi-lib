import axios from 'axios';
import * as common from './Common';

export async function createInvitation(url, alias, auto_accept, multi_use, public_did, body = {}) {
  const param = common.paramGenerator([["alias", alias], ["auto_accept", auto_accept], ["multi_use", multi_use], ["public", public_did]]);
  let data;
  await axios.post(`${url}connections/create-invitation${param}`, body)
    .then(res => {
      data = res.data;
    })
    .catch(err => {
      console.log(err);
      data = undefined;
    })
  return data;
}

export async function receiveInvitation(url, alias, auto_accept, mediation_id, body = {}) {
  const param = common.paramGenerator([["alias", alias], ["auto_accept", auto_accept], ["mediation_id", mediation_id]]);
  let data;
  await axios.post(`${url}connections/receive-invitation${param}`, body)
    .then(res => {
      data = res.data;
      data.status = true;
    })
    .catch(err => {
      console.log(err);
      data.status = true;
      data.error = err;
    })
  return data;
}

export async function getConnections(url, alias, connection_protocol, invitation_key, my_did, state, their_did, their_role) {
  const param = common.paramGenerator([["alias", alias], ["connection_protocol", connection_protocol], ["invitation_key", invitation_key], ["my_did", my_did], ["state", state], ["their_did", their_did], ["their_role", their_role]]);
  let data;
  await axios.get(`${url}connections${param}`)
    .then(res => {
      data = res.data.results;
    })
    .catch(err => {
      console.log(err);
      data = undefined;
    })
  return data;
}

export async function trustPing(url, conn_id, body = {}) {
  let data;
  await axios.post(`${url}connections/${conn_id}/send-ping`, body)
    .then(res => {
      data = res.data;
    })
    .catch(err => {
      console.log(err);
      data = undefined;
    })
  return data;
}

export async function acceptRequest(url, conn_id) {

  let finalUrl = url + 'connections/' + conn_id + '/accept-request'
  let ack;

  await axios.post(finalUrl)
    .then(res => {
      ack = res.data;
    })

  return ack;

};

export function startWebSocketEffect(url, setResults) {

  // starts a web socket, parameters:
  // 
  //    url: string, e.g. REACT_APP_RUTH_HOOK or REACT_APP_CAVENDISH_HOOK etc
  // 
  //    setResult: should be a react state, e.g. calling method should pass setResults of:
  //                                             const [results, setResults] = useState([]);

  return () => {
    const ws = new WebSocket(url + "topic/connections");

    // open the web socket
    ws.onopen = function open() {
      ws.send("web controller connected");
    };

    // run on receipt of web socket message
    ws.onmessage = (e) => {

      // update the results state
      setResults((results) => {
        let temp = [];
        results.map((element) => {
          if (!(JSON.parse(e.data).connection_id === element.connection_id)) {
            temp.push(element);
          }
        });
        temp.push(JSON.parse(e.data));
        temp.sort(function (a, b) {
          return Date.parse(b.updated_at) - Date.parse(a.updated_at);
        });
        return temp;
      });
    };

    // the returned method will be called when react cleaning up
    return () => {
      ws.close();
    };
  };
}

export function fetchConnectionsEffect(url, setResults) {

  // url = e.g. REACT_APP_RUTH_API or REACT_APP_CAVENDISH_API etc
  // setResults = passed in from calling method from const [results, setResults] = useState([]);

  return () => {
    async function fetchData() {
      let data = await getConnections(url, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
      data = await data.filter((element) => {
        if (element.alias === undefined) {
          return true;
        } else {
          if ((element.alias === "public") && (element.rfc23_state === "invitation-sent")) {
            return false;
          } else {
            return true;
          }
        }
      })
      await data.sort(function (a, b) {
        return Date.parse(b.updated_at) - Date.parse(a.updated_at);
      });
      setResults(data);
    }
    fetchData();
  };
}
