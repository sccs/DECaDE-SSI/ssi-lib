import axios from 'axios';

export async function getCredentialExchanges(url) {
    let data;
    await axios.get(`${url}issue-credential-2.0/records`)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendCredentialProposal(url, body = {}) {

    let bodyRaw = {
        autoRemove: true,
        comment: "default comment",
        trace: true,
        ...body
    }
    let data;
    await axios.post(`${url}issue-credential-2.0/send-proposal`, bodyRaw)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendCredentialOffer(url, cred_ex_id, body = {}) {

    let data;
    await axios.post(`${url}issue-credential-2.0/records/${cred_ex_id}/send-offer`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendCredentialOfferDirect(url, body = {}) {
    let data;
    await axios.post(`${url}issue-credential-2.0/send-offer`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function issueCredentials(url, cred_ex_id, body = {}) {
    let data;
    await axios.post(`${url}issue-credential-2.0/records/${cred_ex_id}/issue`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function storeCredentials(url, cred_ex_id, body = {}) {
    let data;
    await axios.post(`${url}issue-credential-2.0/records/${cred_ex_id}/store`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendCredentialRequest(url, cred_ex_id, body = {}) {
    let data;
    await axios.post(`${url}issue-credential-2.0/records/${cred_ex_id}/send-request`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export function startWebSocketEffect(url, setResults) {

    return () => {
        const ws = new WebSocket(url + "topic/issue_credential_v2_0");

        // open the web socket
        ws.onopen = function open() {
            ws.send("web controller connected");
        };

        // run on receipt of web socket message
        ws.onmessage = (e) => {

            // NOTE - passing a lambda to a setState will pass in the associated state of the useState !
            // setResults((results) => {

            setResults((results) => {
                let temp = [];
                results.map((element) => {
                    if (!(JSON.parse(e.data).cred_ex_id === element.cred_ex_id)) {
                        temp.push(element);
                    }
                });
                temp.push(JSON.parse(e.data));
                temp.sort(function (a, b) {
                    return Date.parse(b.updated_at) - Date.parse(a.updated_at);
                });
                return temp;
            });
        };

        // the returned method will be called when react cleans up
        return () => {
            ws.close();
        };
    };
}
