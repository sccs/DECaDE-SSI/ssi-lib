import axios from 'axios';

import * as SSIConnections from './Connections';
import * as SSICredentialExchange from './CredentialExchange';
import * as SSIPresentationsHelper from './PresentationsHelper';

export async function getCredentials(url) {
    let data;
    await axios.get(`${url}credentials`)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function fetchCredentials(url) {
    let connectionsData = await SSIConnections.getConnections(url);
    let credentialsData = await SSICredentialExchange.getCredentialExchanges(url);
    let presentationData = await SSIPresentationsHelper.getPresentationExchanges(url);
    let presentationDataV2 = await SSIPresentationsHelper.getPresentationExchanges_v2(url);

    presentationData.sort(function (a, b) {
        return Date.parse(b.updated_at) - Date.parse(a.updated_at);
    });

    presentationDataV2.sort(function (a, b) {
        return Date.parse(b.updated_at) - Date.parse(a.updated_at);
    });

    let temp = [];
    credentialsData.map((conn) => {
        if (typeof conn.cred_ex_record === typeof {}) {
            temp.push(conn.cred_ex_record);
        } else {
            let tempy = {
                state: "Invalid",
                ...conn,
            };
            temp.push(tempy);
        }
    });

    let credentialData = await getCredentials(url);

    return {
        connectionResults: connectionsData,
        storedCredentials: credentialData,
        pendingCredentials: temp,
        presentationResults: presentationData,
        presentationResultsV2: presentationDataV2,
    }
}
