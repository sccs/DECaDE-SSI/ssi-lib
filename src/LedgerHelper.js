import axios from 'axios';
const common = require('./Common');

export async function getRole(url, did) {
    const param = common.paramGenerator([["did", did]]);
    let data;
    await axios.get(`${url}ledger/get-nym-role${param}`)
        .then(res => {
            data = res.data.role;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function registerPublicDid(url, did, verkey, alias, role) {
    const param = common.paramGenerator([["did", did], ["verkey", verkey], ["alias", alias], ["role", role]]);
    let obj;
    await axios.post(`${url}ledger/register-nym${param}`)
        .then(res => {
            obj = {
                success: true
            }
        })
        .catch(err => {
            console.log(err);
            obj = {
                success: false
            }
        })
    return obj;
}
