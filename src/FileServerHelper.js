const FormData = require('form-data');
const axios = require('axios');

export async function uploadImage(fileServerUrl, file, hash) {
    let data;
    const formData = new FormData();
    formData.append('image', file);
    formData.append('hash', hash);
    await axios.post(`${fileServerUrl}uploadImage`, formData)
        .then(res => {
            data = res.files;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function moveToRegistry(fileServerUrl, filename) {
    let data;
    await axios.get(`${fileServerUrl}moveToRegistry?filename=${filename}`)
        .then(res => {
            data = res;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getImagesFromRegistry(fileServerUrl) {
    let data;
    await axios.get(`${fileServerUrl}imageRegistryFiles`)
        .then(res => {
            data = res.data.files;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function imageHash(fileServerUrl, filename) {
    let data;
    await axios.get(`${fileServerUrl}imageHash?filename=${filename}`)
        .then(res => {
            data = res;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}
