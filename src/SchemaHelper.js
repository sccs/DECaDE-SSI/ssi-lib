const axios = require('axios');
const common = require('./Common');

export async function createSchema(url, conn_id, create_transaction_for_endorser, body = {}) {
    const param = common.paramGenerator([["conn_id", conn_id], ["create_transaction_for_endorser", create_transaction_for_endorser]]);
    let data;
    await axios.post(`${url}schemas${param}`, body)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getSchemaId(url, schema_id) {
    let data;
    await axios.get(`${url}schemas/${schema_id}`)
        .then(res => {
            data = res.data.schema;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getSchemas(url, schema_id, schema_issuer_did, schema_name, schema_version) {
    const param = common.paramGenerator([["schema_id", schema_id], ["schema_issuer_did", schema_issuer_did], ["schema_name", schema_name], ["schema_version", schema_version]]);
    let data;
    await axios.get(`${url}schemas/created${param}`)
        .then(res => {
            data = res.data.schema_ids;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}