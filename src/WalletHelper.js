const axios = require('axios');
const common = require('./Common');

export async function assignPublicDid(url, did) {
    const param = common.paramGenerator([["did", did]]);
    let data;
    await axios.post(`${url}wallet/did/public${param}`)
        .then(res => {
            data = res.data.result;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function createDid(url, body = { "method": "sov", "options": { "key_type": "ed25519" } }) {
    let data;
    await axios.post(`${url}wallet/did/create`, body)
        .then(res => {
            data = res.data.result;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getDids(url) {
    let data;
    await axios.get(`${url}wallet/did`)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getPublicDid(url) {
    let data;
    await axios.get(`${url}wallet/did/public`)
        .then(res => {
            data = res.data.result;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}
