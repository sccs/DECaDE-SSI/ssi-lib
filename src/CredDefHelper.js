const axios = require('axios');
const common = require('./Common');

export async function createCredDef(url, conn_id, create_transaction_for_endorser, body = {}) {
    const param = common.paramGenerator([["conn_id", conn_id], ["create_transaction_for_endorser", create_transaction_for_endorser]]);
    let data;
    await axios.post(`${url}credential-definitions${param}`, body)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getCredDefId(url, cred_def_id) {
    let data;
    await axios.get(`${url}credential-definitions/${cred_def_id}`)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getCredDefs(url, cred_def_id, issuer_did, schema_id, schema_issuer_did, schema_name, schema_version) {
    const param = common.paramGenerator([["cred_def_id", cred_def_id], ["issuer_did", issuer_did], ["schema_id", schema_id], ["schema_issuer_did", schema_issuer_did], ["schema_name", schema_name], ["schema_version", schema_version]]);
    let data;
    await axios.get(`${url}credential-definitions/created${param}`)
        .then(res => {
            data = res.data.credential_definition_ids;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function acceptRequest(url, conn_id) {

    let finalUrl = url + 'connections/' + conn_id + '/accept-request'
    let ack;

    await axios.post(finalUrl)
        .then(res => {
            ack = res.data;
        })

    return ack;
};
