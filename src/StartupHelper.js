import * as ledger from "ssi-lib/LedgerHelper";
import * as wallet from "ssi-lib/WalletHelper";
import * as connection from "ssi-lib/Connections";
import * as endorser from "ssi-lib/EndorserHelper";
import * as schemaHelper from "ssi-lib/SchemaHelper";
import * as cred_def from "ssi-lib/CredDefHelper";

// contains the common denominators of the original webcontroller startup.js
// seem to apply to all issuer / verifier portal setups !

export async function configureAgents(endorserUrl, trustAnchor) {

    console.log('\tConfiguring agents...');

    if (!await ledger.getRole(endorserUrl, (await wallet.getPublicDid(endorserUrl)).did) === "ENDORSER") return;

    let did = (await wallet.getPublicDid(endorserUrl)).did;
    await wallet.assignPublicDid(endorserUrl, trustAnchor.did);
    await wallet.assignPublicDid(endorserUrl, did);

    console.log("\t\tEndorser public did assigned.");
}

export async function displayDetails(endorserURL, agentURL) {

    console.log('\tEndorser: ' + await ledger.getRole(endorserURL, (await wallet.getPublicDid(endorserURL)).did) + " " + (await wallet.getPublicDid(endorserURL)).did);
    console.log('\This Agent: ' + await ledger.getRole(agentURL, (await wallet.getPublicDid(agentURL)).did) + " " + (await wallet.getPublicDid(agentURL)).did);
}

export async function configureConnections() {

    console.log('\tConfiguring connections...');
}

export async function configureEndorser(url, endorserUrl) {

    console.log('\tConfiguring endorsers...');

    const endorserConnections = await connection.getConnections(endorserUrl);
    for (let conn in endorserConnections) await endorser.setEndorserRole(endorserUrl, endorserConnections[conn].connection_id, "TRANSACTION_ENDORSER");

    const endorserConnection = (await connection.getConnections(url, "endorser"))[0];

    await endorser.setEndorserRole(url, endorserConnection.connection_id, "TRANSACTION_AUTHOR");

    const endorserDid = (await wallet.getPublicDid(endorserUrl)).did;
    await endorser.setEndorserInfo(url, endorserConnection.connection_id, endorserDid);

    return endorserConnection.connection_id;
}

export async function configureSchemas(url, schema, endorserConnectionId, endorserUrl, ws) {

    console.log('\tConfiguring schemas...');

    const schemas = await schemaHelper.getSchemas(url);

    let flag = false;
    for (let i in schemas) {
        const sch = await schemaHelper.getSchemaId(url, schemas[i]);
        try {
            if (sch.name === schema.schema_name) flag = true;
        } catch (e) { }
    }

    if (flag) return; // we're done

    // no schema found, create one !
    const txn = await schemaHelper.createSchema(url, endorserConnectionId, true, schema);

    await endorser.endorserCreateRequest(url, txn.txn.transaction_id, true);

    await new Promise(resolve => {
        ws.onmessage = async (e) => {
            let state = JSON.parse(e.data).state;
            let threadId = JSON.parse(e.data).thread_id;

            if ((state === "request_received") && (threadId === txn.txn.transaction_id)) {
                await endorser.endorseTransactions(endorserUrl, JSON.parse(e.data).transaction_id)
                resolve();
            }
        }
    })
}

export async function configureCredDefs(url, schema, endorserConnectionId, endorserUrl, ws) {

    console.log('\tConfiguring credential definitions...');

    const schemas = await schemaHelper.getSchemas(url);
    let schemaSeqNo;
    let schemaId;
    for (let i in schemas) {
        const sch = await schemaHelper.getSchemaId(url, schemas[i]);
        try {
            if (sch.name === schema.schema_name) {
                schemaSeqNo = sch.seqNo;
                schemaId = sch.id;
            }
        } catch (e) { }
    }

    const credDefs = await cred_def.getCredDefs(url);
    let flag = false;
    for (let i in credDefs) {
        const crd = await cred_def.getCredDefId(url, credDefs[i]);
        if (parseInt(crd.credential_definition.schemaId) === schemaSeqNo) {
            flag = true;
        }
    }

    if (flag) return; // we're done

    const txn = await cred_def.createCredDef(url, endorserConnectionId, true, { "schema_id": schemaId });
    await endorser.endorserCreateRequest(url, txn.txn.transaction_id, true);
    await new Promise(resolve => {
        ws.onmessage = async (e) => {
            let state = JSON.parse(e.data).state;
            let threadId = JSON.parse(e.data).thread_id;
            if ((state === "request_received") && (threadId === txn.txn.transaction_id)) {
                await endorser.endorseTransactions(endorserUrl, JSON.parse(e.data).transaction_id)
                resolve();
            }
        }
    })
}

export async function configureGlobalStore(url, schema) {

    console.log('\tConfiguring the global store...');

    const schemas = await schemaHelper.getSchemas(url);
    let schemaSeqNo;
    let shm;
    for (let i in schemas) {
        const sch = await schemaHelper.getSchemaId(url, schemas[i]);
        try {
            if (sch.name === schema.schema_name) {
                schemaSeqNo = sch.seqNo;
                shm = sch;
            }
        } catch (e) { }
    }

    const credDefs = await cred_def.getCredDefs(url);
    let cdr;
    for (let i in credDefs) {
        const crd = await cred_def.getCredDefId(url, credDefs[i]);
        if (parseInt(crd.credential_definition.schemaId) === schemaSeqNo) cdr = crd;
    }
    let did = await wallet.getPublicDid(url);

    let obj = {
        certification: {
            schema_name: shm.name,
            schema_version: shm.version,
            schema_id: shm.id,
            cred_def_id: cdr.credential_definition.id,
            issuer_did: did.did,
            schema_issuer_did: did.did
        }
    }

    console.log("universalObject:", obj);

    return obj;
}
