/*

    *** ATTENTION ***

        every time the von network is removed and restarted
        the following certifcates will need to be updated.

        to do this, check the "certification" details in the console
        on running Cavendish and Thoday agents

    *** TIP ***

        i find the easiest way to do the copying is right click in the
        chrome console and select "copy object", then just select { ... }
        below and ctrl+v to paste.

*/

export const certifications = {
    photographerCertification: {
        "schema_name": "PhotographerCertification",
        "schema_version": "1.0",
        "schema_id": "PxNTs3yPSL3SD9e7DpAqF2:2:PhotographerCertification:1.0",
        "cred_def_id": "PxNTs3yPSL3SD9e7DpAqF2:3:CL:170:default",
        "issuer_did": "PxNTs3yPSL3SD9e7DpAqF2",
        "schema_issuer_did": "PxNTs3yPSL3SD9e7DpAqF2"
    },
    imageAuthorshipCertification: {
        "schema_name": "ImageAuthorship",
        "schema_version": "1.0",
        "schema_id": "J49EvjrfvBhmucir4M466L:2:ImageAuthorship:1.0",
        "cred_def_id": "J49EvjrfvBhmucir4M466L:3:CL:169:default",
        "issuer_did": "J49EvjrfvBhmucir4M466L",
        "schema_issuer_did": "J49EvjrfvBhmucir4M466L"
    }
}
