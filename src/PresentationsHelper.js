const axios = require('axios');

export async function sendPresentationProposal(url, body = {}) {

    let bodyRaw = {
        trace: true,
        auto_present: false,
        ...body
    }
    let data;
    await axios.post(`${url}present-proof/send-proposal`, bodyRaw)
        .then(res => {
            data = res.data.result;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendPresentProof_2_0_Request(url, body = {}) {

    /*

      based on:
    
         https://github.com/hyperledger/aries-cloudagent-python/blob/main/demo/AriesOpenAPIDemo.md#requestingpresenting-a-proof
          
      this is different to the original web controller. here we're using the v2 endpoints
    
    */

    let bodyRaw = {
        trace: true,
        ...body
    }
    let data;
    await axios.post(`${url}present-proof-2.0/send-request`, bodyRaw)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function sendPresentationRequest(url, body = {}, pres_ex_id = undefined) {
    let bodyRaw = {
        trace: true,
        ...body
    }
    let data;
    if (pres_ex_id !== undefined) {
        await axios.post(`${url}present-proof/records/${pres_ex_id}/send-request`, bodyRaw)
            .then(res => {
                data = res.data.results;
            })
            .catch(err => {
                console.log(err);
                data = undefined;
            })
    }
    else {
        console.log("--> ${url}present-proof/send-request");
        await axios.post(`${url}present-proof/send-request`, bodyRaw)
            .then(res => {
                data = res.data.results;
            })
            .catch(err => {
                console.log(err);
                data = undefined;
            })
    }
    return data;
}

export async function sendPresentation(url, pres_ex_id, body = {}) {
    let data;
    await axios
        .post(`${url}present-proof/records/${pres_ex_id}/send-presentation`, body)
        .then((res) => {
            data = res.data;
        })
        .catch((err) => {
            console.log(err);
            data = undefined;
        });
    return data;
}

export async function verifyPresentation(url, pres_ex_id, body = {}) {
    let data;
    await axios.post(`${url}present-proof/records/${pres_ex_id}/verify-presentation`, body)
        .then(res => {
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function verifyPresentation_v2(url, pres_ex_id, body = {}) {
    let data;
    await axios.post(`${url}present-proof-2.0/records/${pres_ex_id}/verify-presentation`, body)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getPresentationExchanges(url) {
    let data;
    await axios
        .get(`${url}present-proof/records`)
        .then((res) => {
            data = res.data.results;
        })
        .catch((err) => {
            console.log(err);
            data = undefined;
        });
    return data;
}

export async function getPresentationExchanges_v2(url) {
    let data;
    await axios
        .get(`${url}present-proof-2.0/records`)
        .then((res) => {
            data = res.data.results;
        })
        .catch((err) => {
            console.log(err);
            data = undefined;
        });
    return data;
}

export function startWebSocketEffect(url, setResults) {

    return () => {
        const ws = new WebSocket(url + "topic/present_proof");

        ws.onopen = function open() {
            ws.send("web controller connected");
        };

        ws.onmessage = (e) => {
            setResults((results) => {
                let temp = [];
                results.map((element) => {
                    if (
                        !(
                            JSON.parse(e.data).presentation_exchange_id ===
                            element.presentation_exchange_id
                        )
                    ) {
                        temp.push(element);
                    }
                });
                temp.push(JSON.parse(e.data));
                temp.sort(function (a, b) {
                    return Date.parse(b.updated_at) - Date.parse(a.updated_at);
                });
                return temp;
            });
        };

        return () => {
            ws.close();
        };
    }
}

export function startWebSocketEffect_v2(url, setResults) {

    return () => {
        const ws = new WebSocket(url + "topic/present_proof_v2_0");

        ws.onopen = function open() {
            ws.send("web controller v2 connected");
        };

        ws.onmessage = (e) => {
            setResults((results) => {
                let temp = [];
                results.map((element) => {
                    if (
                        !(
                            JSON.parse(e.data).presentation_exchange_id ===
                            element.presentation_exchange_id
                        )
                    ) {
                        temp.push(element);
                    }
                });
                temp.push(JSON.parse(e.data));
                temp.sort(function (a, b) {
                    return Date.parse(b.updated_at) - Date.parse(a.updated_at);
                });
                return temp;
            });
        };

        return () => {
            ws.close();
        };
    }
}

export async function sendProofPresentation_v2(url, pres_ex_id, body = {}) {
    let data;
    await axios
        .post(`${url}present-proof-2.0/records/${pres_ex_id}/send-presentation`, body)
        .then((res) => {
            data = res.data;
        })
        .catch((err) => {
            console.log(err);
            data = undefined;
        });
    return data;
}
