const axios = require('axios');
const common = require('./Common');

export async function endorserCreateRequest(url, tran_id, endorser_write_txn, body = { "expires_time": "2025-03-29T05:22:19Z" }) {
    const param = common.paramGenerator([["tran_id", tran_id], ["endorser_write_txn", endorser_write_txn]]);
    let data;
    await axios.post(`${url}transactions/create-request${param}`, body)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function endorseTransactions(url, tran_id) {
    let data;
    await axios.post(`${url}transactions/${tran_id}/endorse`)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function getEndorserTransactions(url, thread_id) {
    let data;
    await axios.get(`${url}transactions`)
        .then(res => {
            // thread_id=undefined;
            // if(thread_id===undefined){
            //   data = res.data;
            // } else {
            //   let temp = (res.data.results).filter((val) => { return (val.thread_id === thread_id)});
            //   data = temp;
            // }
            data = res.data.results;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function refuseTransactions(url, tran_id) {
    let data;
    await axios.post(`${url}transactions/${tran_id}/refuse`)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function setEndorserInfo(url, conn_id, endorser_did) {
    const param = common.paramGenerator([["endorser_did", endorser_did]]);
    let data;
    await axios.post(`${url}transactions/${conn_id}/set-endorser-info${param}`)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}

export async function setEndorserRole(url, conn_id, transaction_my_job) {
    const param = common.paramGenerator([["transaction_my_job", transaction_my_job]]);
    let data;
    await axios.post(`${url}transactions/${conn_id}/set-endorser-role${param}`)
        .then(res => {
            data = res.data;
        })
        .catch(err => {
            console.log(err);
            data = undefined;
        })
    return data;
}
